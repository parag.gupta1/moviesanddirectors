import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import axios from "axios";
import "./App.css";

class Directors extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      directors: [],
      isSearch: false,
      searchedDirectorData: null,
      updateModalStyle: false,
      updateDirector: []
    };
  }
  componentDidMount() {
    axios
      .get("http://localhost:5000/api/directors/detail")
      .then(response => {
        console.log(response.data);
        this.setState({ directors: response.data.result });
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleDelete(id, key) {
    if (this.state.isSearch) {
      let newDirectorData = this.state.searchedDirectorData.slice();
      newDirectorData.splice(key, 1);
      this.setState({ searchedDirectorData: newDirectorData });
    }

    axios
      .delete(`http://localhost:5000/api/directors/${id}`)
      .then(response => {
        console.log(response.data);
        // this.setState({ directors: response.data.result });    searchedDirectorData:[], isSearch:false
        axios
          .get("http://localhost:5000/api/directors/detail")
          .then(response => {
            console.log(response.data);
            this.setState({ directors: response.data.result });
          })
          .catch(error => {
            console.log(error);
          });
        // alert("Deleted");
      })
      .catch(error => {
        console.log(error);
      });
  }

  search(e) {
    // this.setState({searchedDirectorData:this.state.directors  })
    let directors = this.state.directors.slice();
    let filterdDirectorData = directors.filter(director => {
      return director["director"]
        .toLowerCase()
        .includes(e.target.value.toLowerCase());
    });
    if (e.target.value !== "")
      this.setState({
        searchedDirectorData: filterdDirectorData,
        isSearch: true
      });
    else this.setState({ searchedDirectorData: [], isSearch: false });
  }
  render() {
    let directors = this.state.isSearch
      ? this.state.searchedDirectorData
      : this.state.directors;

    // console.log('parag')
    // console.log(this.props.match.path)
    return (
      <div>
        <div id="search" className="form-group">
          {/* <button className={`btn btn-info form-control form-group`}>*/}
          <Link
            className={`btn btn-primary form-control form-group`}
            to={`${this.props.match.url}/add`}
          >
            Add New Director
          </Link>
          {/*</button>*/}
          <input
            className="form-control"
            name="searchbox"
            placeholder="Search by director name"
            onChange={e => {
              this.search(e);
            }}
          ></input>
        </div>
        <table className={`table table-hover`}>
          <thead className="thead-light">
            <tr>
              <th>Serial</th>
              <th>Director</th>
              <th>Movies</th>
              <th>Total movies</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {directors.length ? (
              directors.map((item, key) => {
                return (
                  <tr id={item.id} key={key}>
                    <Link
                      className="nolink"
                      style={{ display: "contents" }}
                      to={`${this.props.match.url}/${item.id}`}
                    >
                      <td>{key + 1 || "NO DATA"}</td>
                      <td>{item.director || "NO DATA"}</td>
                      <td>{item.movies || "NO DATA"}</td>
                      <td>{item.movieCount || "NO DATA"}</td>
                    </Link>
                    <td>
                      {/*<button onClick={e => this.handleUpdate(item.id, key, e)}>
                        Update
                      </button>
                      <br />*/}
                      <button
                        className="btn btn-warning"
                        onClick={e => this.handleDelete(item.id, key, e)}
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              })
            ) : (
              <span>No Data Present....</span>
            )}
          </tbody>
        </table>
        {/*<UpdateModal display={this.state.updateModalStyle ? "block-display" : "no-display"}
          updateDirector={(user, id, e) => {
            this.state.updateDirector.length
              ? this.updateDirector(user, id, e)
              : this.addDirector(user, id, e);
          }}
          closeModal={() => {
            this.closeModal();
          }}
          data={this.state.updateDirector}/>
                   <Router>
         <Route path={`${this.props.match.path}/:id`} render={() => <h3>It is not working</h3>} />
         </Router>*/}
      </div>
    );
  }
}

export default Directors;
