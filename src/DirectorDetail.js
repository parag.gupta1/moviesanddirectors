import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import axios from "axios";
import "./App.css";

export default class MovieDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      director: this.props.isfor == "update" ? null : []
    };
  }
  componentDidMount() {
    // axios.get('https://dog.ceo/api/breeds/image/random')
    if (this.props.isfor == "update")
      axios
        .get(
          `http://localhost:5000/api/directors/${this.props.match.params.id}`
        )
        .then(response => {
          console.log(response.data);
          this.setState({
            director: response.data.success ? response.data.result : []
          });
          // return(response.data);
        })
        .catch(error => {
          console.log(error);
        });
  }
  handleUpdate(id, event) {
    event.preventDefault();
    // console.log(event.target.elements.title.value) // from elements property
    // console.log(event.target.genre.value); // or directly
    let data = {
      director: event.target.director.value
    };

    if (this.props.isfor == "update") {
      axios
        .put("http://localhost:5000/api/directors/" + id, data)
        .then(response => {
          console.log(response);
          // alert("Successful Updated");
          // this.setState({ directors: response.data.result });
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      axios
        .post("http://localhost:5000/api/directors", data)
        .then(response => {
          console.log(response);
          // alert("Successful Added");
          // this.setState({ directors: response.data.result });
        })
        .catch(error => {
          console.log(error);
        });
    }

    this.props.history.push("");
    // this.props.history.goBack();
  }
  render() {
    console.log(this.props);
    let director = this.state.director;
    console.log(director);
    return (
      <>
        {director ? (
          <div style={{ textAlign: "center" }}>
            <form
              className="form-group"
              onSubmit={e => {
                this.handleUpdate(
                  this.props.isfor == "update" ? director[0].id : 0,
                  e
                );
              }}
            >
              <input
                className={`update-input`}
                name="director"
                type="text"
                defaultValue={
                  director[0]
                    ? director[0].director || "NO DATA"
                    : "Enter Director Name"
                }
              />

              <input
                className={`btn-secondary`}
                type="submit"
                value={this.props.isfor == "update" ? "Update" : "Add"}
              ></input>

              {/*<button onClick={e => this.handleDelete(e)}>
                      Delete
                    </button>*/}
            </form>
          </div>
        ) : (
          <span>No Data Present....</span>
        )}
      </>
    );
  }
}
