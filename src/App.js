import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Movies from "./Movies";
import MovieDetail from "./MovieDetail";
import DirectorDetail from "./DirectorDetail";
import Directors from "./Directors";

// import axios from 'axios';
//match= {"path":"/about","url":"/about","isExact":true,"params":{}}
//location ={"pathname":"/movies/2","search":"","hash":"","key":"a16ljy"}

function App() {
  return (
    <Router>
      <div>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/movies" component={Movies} />
          <Route
            exact
            path="/movies/add"
            render={props => <MovieDetail {...props} isfor="add" />}/>
          <Route
            exact
            path="/directors/add"
            render={props => <DirectorDetail {...props} isfor="add" />}/>
          <Route exact path="/directors" component={Directors} />
          <Route exact path="/about" component={About} />
          <Route exact path="/topics" component={Topics} />
          <Route
            exact
            path="/test"
            render={({ location }) => <h3>It is working </h3>}/>

          <Route
            exact
            path="/movies/:id"
            render={props => <MovieDetail {...props} isfor="update" />}/>
          <Route
            exact
            path="/directors/:id"
            render={props => <DirectorDetail {...props} isfor="update" />}/>
          <Route component={NoMatch} />
        </Switch>
      </div>
    </Router>
  );
}

function Home() {
  return <h2>Home</h2>;
}
// function Movies() {
//   return <h2>Movies</h2>;
// }
// function Directors() {
//   const [directorData, setDirectorsData] = useState("directorData");
//   const [updateModalStyle, setUpdateModalStyle] = useState(false);
//   const handleClick = () => {
//     setDirectorsData("myUpdatedValue");
//   };
//   useEffect(() => {
//     console.log("rendered");
//   }, []);

//   return <h2>Directors</h2>;
// }

function About({ match }) {
  return <h2>{`match:${JSON.stringify(match)}`}</h2>;
}

function Topic({ match }) {
  return <h3>Requested Param: {match.params.id}</h3>;
}

function Topics({ match }) {
  return (
    <div>
      <h2>Topics</h2>
      <ul>
        <li>
          <Link to={`${match.url}/components`}>Components</Link>
        </li>
        <li>
          <Link to={`${match.url}/props-v-state`}>Props v. State</Link>
        </li>
      </ul>

      <Route path={`${match.path}/:id`} component={Topic} />
      <Route
        exact
        path={match.path}
        render={() => <h3>Please select a topic.</h3>}
      />
    </div>
  );
}

function Header() {
  return (
    <header className="form-group">
      <ul>
        <li>
          <Link className="btn btn-primary" to="/">
            Home
          </Link>
        </li>
        <li>
          <Link className="btn btn-primary" to="/movies">
            Movies
          </Link>
        </li>
        <li>
          <Link className="btn btn-primary" to="/directors">
            Directors
          </Link>
        </li>
        <li>
          <Link className="btn btn-primary" to="/about">
            About
          </Link>
        </li>
        <li>
          <Link className="btn btn-primary" to="/topics">
            Topics
          </Link>
        </li>
      </ul>
    </header>
  );
}
function NoMatch({ location }) {
  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

export default App;
