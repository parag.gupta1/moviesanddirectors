import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import axios from "axios";
import "./App.css";

export default class MovieDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      movie: this.props.isfor == "update" ? null : {},
      directorList: []
    };
  }
  componentDidMount() {
    // axios.get('https://dog.ceo/api/breeds/image/random')
    if (this.props.isfor == "update")
      axios
        .get(`http://localhost:5000/api/movies/${this.props.match.params.id}`)
        .then(response => {
          console.log(response.data);
          this.setState({
            movie: response.data.success ? response.data.result : []
          });
          // return(response.data);
        })
        .catch(error => {
          console.log(error);
        });

    axios
      .get(`http://localhost:5000/api/directors`)
      .then(response => {
        console.log(response.data);
        this.setState({ directorList: response.data.result });
      })
      .catch(error => {
        console.log(error);
      });
  }
  handleUpdate(id, event) {
    event.preventDefault();
    // console.log(event.target.elements.title.value) // from elements property
    console.log(event.target.genre.value); // or directly
    let data = {
      rank: event.target.rank.value,
      title: event.target.title.value,
      description: event.target.description.value,
      runtime: event.target.runtime.value,
      genre: event.target.genre.value,
      rating: event.target.rating.value,
      metascore: event.target.metascore.value,
      votes: event.target.votes.value,
      grossEarningInMil: event.target.grossEarningInMil.value,
      directorId: event.target.directorId.value,
      actor: event.target.actor.value,
      year: event.target.year.value
      // :event.target..value,
    };

    if (this.props.isfor == "update") {
      axios
        .put("http://localhost:5000/api/movies/" + id, data)
        .then(response => {
          console.log(response);
          // alert("Successful Updated");
          // this.setState({ movies: response.data.result });
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      axios
        .post("http://localhost:5000/api/movies", data)
        .then(response => {
          console.log(response);
          // alert("Successful Added");
          // this.setState({ movies: response.data.result });
        })
        .catch(error => {
          console.log(error);
        });
    }

    // if (numberKey >= 0) {
    //   let id = this.state.movies[numberKey]["id"];
    //   let Rank = this.state.movies[numberKey]["first_name"];
    //   let lastName = this.state.movies[numberKey]["last_name"];
    //   let email = this.state.movies[numberKey]["email"];
    //   let avatar = this.state.movies[numberKey]["avatar_url"];
    //   let phone = this.state.movies[numberKey]["phone"];
    //   this.setState({
    //     updateModalStyle: true,
    //     updateMovie: [id, Rank, lastName, email, avatar, phone]
    //   });
    // } else {
    //   this.setState({
    //     updateModalStyle: true,
    //     // updateMovie:['id', 'Rank', 'lastName', 'email', 'image url', 'phone'],
    //     updateMovie: []
    //   });
    // }
  }
  render() {
    // console.log("MD rendered");
    console.log(this.props);
    let movie = this.state.movie;
    let directorList = this.state.directorList;
    console.log(movie);
    return (
      <>
        {movie ? (
          <div>
            <form
              className="form-group"
              onSubmit={e => this.handleUpdate(movie[0].id, e)}
            >
              <table
                id={movie[0].id}
                style={{ textAlign: "center" }}
                className={`table table-hover`}
              >
                <tr>
                  <td width="25%">Rank</td>
                  <td>
                    <input
                      className={`update-input form-control`}
                      name="rank"
                      required="true"
                      type="number"
                      min="0"
                      defaultValue={movie[0].Rank || "NO DATA"}
                    />
                  </td>
                </tr>
                <tr>
                  <td width="25%">Title</td>
                  <td>
                    <input
                      className={`update-input form-control`}
                      name="title"
                      required="true"
                      type="text"
                      minLength="2"
                      defaultValue={movie[0].Title || "NO DATA"}
                    />
                  </td>
                </tr>
                <tr>
                  <td width="25%">Description</td>
                  <td>
                    <textarea
                      className={`form-control`}
                      name="description"
                      minLength="25"
                      style={
                        {
                          /*
                        minHeight: "100px",
                        minWidth: "166px",
                        maxWidth: "166px"*/
                        }
                      }
                      defaultValue={movie[0].Description || "NO DATA"}
                    ></textarea>
                  </td>
                </tr>
                <tr>
                  <td width="25%">Runtime</td>
                  <td>
                    <input
                      className={`update-input form-control`}
                      name="runtime"
                      required="true"
                      type="number"
                      min="0"
                      defaultValue={movie[0].Runtime || "NO DATA"}
                    />
                  </td>
                </tr>
                <tr>
                  <td width="25%">Genre</td>
                  <td>
                    <select
                      className={`update-input form-control`}
                      required
                      name="genre"
                      defaultValue={movie[0].Genre}
                    >
                      <option value="0">select genere</option>
                      <option
                        selected={movie[0].Genre === "Crime" ? "selected" : ""}
                      >
                        Crime
                      </option>
                      <option
                        selected={movie[0].Genre === "Action" ? "selected" : ""}
                      >
                        Action
                      </option>
                      <option
                        selected={
                          movie[0].Genre === "Biography" ? "selected" : ""
                        }
                      >
                        Biography
                      </option>
                      <option
                        selected={
                          movie[0].Genre === "Adventure" ? "selected" : ""
                        }
                      >
                        Adventure
                      </option>
                      <option
                        selected={
                          movie[0].Genre === "Western" ? "selected" : ""
                        }
                      >
                        Western
                      </option>
                      <option
                        selected={movie[0].Genre === "Drama" ? "selected" : ""}
                      >
                        Drama
                      </option>
                      <option
                        selected={
                          movie[0].Genre === "Animation" ? "selected" : ""
                        }
                      >
                        Animation
                      </option>
                      <option
                        selected={movie[0].Genre === "Comedy" ? "selected" : ""}
                      >
                        Comedy
                      </option>
                      <option
                        selected={movie[0].Genre === "Horror" ? "selected" : ""}
                      >
                        Horror
                      </option>
                      <option
                        selected={
                          movie[0].Genre === "Mystery" ? "selected" : ""
                        }
                      >
                        Mystery
                      </option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td width="25%">Rating</td>
                  <td>
                    <input
                      className={`update-input form-control`}
                      name="rating"
                      required="true"
                      step="any"
                      type="number"
                      min="0"
                      max="10"
                      defaultValue={movie[0].Rating || "NO DATA"}
                    />
                  </td>
                </tr>
                <tr>
                  <td width="25%">Metascore</td>
                  <td>
                    <input
                      className={`update-input form-control`}
                      name="metascore"
                      required="true"
                      min="0"
                      max="100"
                      type="number"
                      defaultValue={movie[0].Metascore || "NO DATA"}
                    />
                  </td>
                </tr>
                <tr>
                  <td width="25%">Votes</td>
                  <td>
                    <input
                      className={`update-input form-control`}
                      name="votes"
                      required="true"
                      type="number"
                      defaultValue={movie[0].Votes || "NO DATA"}
                    />
                  </td>
                </tr>
                <tr>
                  <td width="25%">Gross_Earning_in_Mil</td>
                  <td>
                    <input
                      className={`update-input form-control`}
                      name="grossEarningInMil"
                      required="true"
                      type="number"
                      step="any"
                      min="0"
                      defaultValue={movie[0].Gross_Earning_in_Mil || "NO DATA"}
                    />
                  </td>
                </tr>
                <tr>
                  <td width="25%">Director</td>
                  <td>
                    <select
                      className={`update-input form-control`}
                      name="directorId"
                      defaultValue={
                        movie[0]["director.director"] || movie[0].Director
                      }
                    >
                      {directorList.map(director => {
                        return (
                          <option
                            value={director.id}
                            selected={
                              (movie[0]["director.id"] ||
                                movie[0].DirectorId) === director.id
                                ? "selected"
                                : ""
                            }
                          >
                            {director.director}
                          </option>
                        );
                      })}
                    </select>
                  </td>
                </tr>
                <tr>
                  <td width="25%">Actor</td>
                  <td>
                    <input
                      className={`update-input form-control`}
                      name="actor"
                      type="text"
                      defaultValue={movie[0].Actor || "NO DATA"}
                    />
                  </td>
                </tr>
                <tr>
                  <td width="25%">Year</td>
                  <td>
                    <input
                      className={`update-input form-control`}
                      name="year"
                      required="true"
                      type="number"
                      min="1960"
                      max="2050"
                      defaultValue={movie[0].Year || "NO DATA"}
                    />
                  </td>
                </tr>
              </table>
              <input
                style={{ float: "right" }}
                className={`update-input form-group btn btn-secondary`}
                type="submit"
                value={this.props.isfor == "update" ? "Update" : "Add"}
              />
              {/*<button onClick={e => this.handleDelete(e)}>
                      Delete
                    </button>*/}
            </form>
          </div>
        ) : (
          <span>No Data Present....</span>
        )}
      </>
    );
  }
}
