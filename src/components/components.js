import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

function Comp() {
  const [myKey, setMyKey] = useState("myInitValue");
  const handleClick = () => {
    setMyKey("myUpdatedValue");
  };
  useEffect(() => {
    console.log("rendered");
  }, [myKey]);
  return (
    <>
      <button
        onClick={e => {
          handleClick(e);
        }}
      >
        {myKey}
      </button>
    </>
  );
}
