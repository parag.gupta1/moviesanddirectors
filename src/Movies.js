import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import axios from "axios";
import "./App.css";

class Movies extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: [],
      isSearch: false,
      searchedMovieData: null,
      updateModalStyle: false,
      updateMovie: []
    };
  }
  componentDidMount() {
    axios
      .get("http://localhost:5000/api/movies")
      .then(response => {
        console.log(response.data);
        this.setState({ movies: response.data.result });
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleDelete(id, key) {
    if (this.state.isSearch) {
      let newMovieData = this.state.searchedMovieData.slice();
      newMovieData.splice(key, 1);
      this.setState({ searchedMovieData: newMovieData });
    }

    axios
      .delete(`http://localhost:5000/api/movies/${id}`)
      .then(response => {
        console.log(response.data);
        // this.setState({ movies: response.data.result });    searchedMovieData:[], isSearch:false
        axios
          .get("http://localhost:5000/api/movies")
          .then(response => {
            console.log(response.data);
            this.setState({ movies: response.data.result });
          })
          .catch(error => {
            console.log(error);
          });
        alert("Deleted");
      })
      .catch(error => {
        console.log(error);
      });
  }

  search(e) {
    // this.setState({searchedMovieData:this.state.movies  })
    let movies = this.state.movies.slice();
    let filterdMovieData = movies.filter(movie => {
      return movie["Title"]
        .toLowerCase()
        .includes(e.target.value.toLowerCase());
    });
    if (e.target.value !== "")
      this.setState({ searchedMovieData: filterdMovieData, isSearch: true });
    else this.setState({ searchedMovieData: [], isSearch: false });
  }
  render() {
    let movies = this.state.isSearch
      ? this.state.searchedMovieData
      : this.state.movies;

    // console.log('parag')
    // console.log(this.props.match.path)
    return (
      <div>
        <div id="search" className="form-group">
          {/* <button className={`btn btn-info form-control form-group`}>*/}
          <Link
            className={`btn btn-primary form-control form-group`}
            to={`${this.props.match.url}/add`}
          >
            Add New Movie
          </Link>
          {/*</button>*/}
          <input
            className="form-control"
            name="searchbox"
            placeholder="Search by movie name"
            onChange={e => {
              this.search(e);
            }}
          ></input>
        </div>
        <table className={`table table-hover`}>
          <thead className="thead-light">
            <tr>
              <th>Rank</th>
              <th>Title</th>
              <th>Rating</th>
              <th colSpan={2}>Action</th>
            </tr>
          </thead>
          <tbody>
            {movies.length ? (
              movies.map((item, key) => {
                return (
                  <tr id={item.id} key={key}>
                    <Link
                      className="nolink"
                      style={{ display: "contents" }}
                      to={`${this.props.match.url}/${item.id}`}
                    >
                      <td>{item.Rank || "NO DATA"}</td>
                      <td>{item.Title || "NO DATA"}</td>
                      <td>{item.Rating || "NO DATA"}</td>
                    </Link>
                    <td>
                      {/*<button onClick={e => this.handleUpdate(item.id, key, e)}>
                        Update
                      </button>
                      <br />*/}
                      <button
                        className="btn btn-warning"
                        onClick={e => this.handleDelete(item.id, key, e)}
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              })
            ) : (
              <span>No Data Present....</span>
            )}
          </tbody>
        </table>
        {/*<UpdateModal display={this.state.updateModalStyle ? "block-display" : "no-display"}
          updateMovie={(user, id, e) => {
            this.state.updateMovie.length
              ? this.updateMovie(user, id, e)
              : this.addMovie(user, id, e);
          }}
          closeModal={() => {
            this.closeModal();
          }}
          data={this.state.updateMovie}/>
                   <Router>
         <Route path={`${this.props.match.path}/:id`} render={() => <h3>It is not working</h3>} />
         </Router>*/}
      </div>
    );
  }
}

export default Movies;
